package com.dalircode.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;
import java.time.LocalDate;
import java.time.Period;

@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String email;
    private String address;
    private LocalDate birthday;

    public Customer() {
    }

    protected Customer(String name, String email, String address) {
        this.name = name;
        this.email = email;
        this.address = address;
    }
    public int getAge(){
        LocalDate now=LocalDate.now();
        Period diff = Period.between(this.birthday,now);

        return diff.getYears();
    };

}

