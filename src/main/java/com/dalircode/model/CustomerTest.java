package com.dalircode.model;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.time.LocalDate;
import java.time.Period;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CustomerTest {
    @Mock
    private Customer customer;
    @Test
    public void getAgeTest(){
        LocalDate date = LocalDate.of(1995,12,01);
        LocalDate now = LocalDate.of(1999,11,04);
        assertEquals(Period.between(date,now).getYears(),3);
    }
    @Test
   public void get_customer_age_test(){
        Customer customer = mock(Customer.class);
        when(customer.getAge()).thenReturn(2);
        assertEquals(2, customer.getAge());
    }

}
